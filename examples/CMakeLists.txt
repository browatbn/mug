cmake_minimum_required(VERSION 2.8 FATAL_ERROR)

project(MugExamples)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/../cmake)

find_package(Eigen REQUIRED)

include_directories(${EIGEN_INCLUDE_DIRS})
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../include)
include_directories(${CMAKE_CURRENT_SOURCE_DIR}/../3rdparty)

add_executable(train_model ${CMAKE_CURRENT_SOURCE_DIR}/train_model.cpp)
target_link_libraries(train_model libmug.a)

