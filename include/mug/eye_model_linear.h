/* 
 * Software License Agreement (BSD License) 
 *
 * MUG - Mobile and Unrestrained Gazetracking
 * Copyright (c) 2013, Max Planck Institute for Biological Cybernetics
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holder nor the names of its 
 *       contributors may be used to endorse or promote products derived from 
 *       this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef LINEAR_EYE_MODEL_H
#define LINEAR_EYE_MODEL_H

#include <vector>
#include <Eigen/Dense>

#include <mug/eye_model.h>

namespace mug
{
    /** 
     * \brief   Eye model using linear regression to create linear mapping from  
     *          pupil positions to gaze angles. 
     * \note    This model is inaccurate for large eye movements but 
     *          computationally very efficient. 
     * \author  Bjoern Browatzki
     */
    class EyeModelLinear : public EyeModel
    {
    public:
        /** 
         * \brief Perform linear regression to fit pupil positions to gaze angles (yaw, pitch).
         * \param[in] pupilPositions Vector of 2D positions in the eye tracker camera image 
         * \param[in] gazeAngles Vector of (yaw, pitch) tuples
         */
        void fit(const std::vector<Eigen::Vector2f> &pupilPositions,  
                 const std::vector<Eigen::Vector2f> &gazeAngles);

        /** 
         * \brief Predict gaze angles based on pupil position based on linear model.
         * \param[in] pupil UV pupil position. Pupil is expected to be 2D vector (monocular).
         * \param[out] 2D vector containing yaw and pitch angle in radians.
         */
        inline Eigen::Vector2f predict(const Eigen::VectorXf &pupil) const 
        {
            return Eigen::Vector2f(
                    modelYaw[0]   * pupil[0] + modelYaw[1],
                    modelPitch[0] * pupil[1] + modelPitch[1] );
        };

    private:
        Eigen::Vector2f modelYaw;       /// coefficients of linear model for yaw 
        Eigen::Vector2f modelPitch;     /// coefficients of linear model for pitch
       
        /** 
         * \brief Computes simple linear regression.
         * \param[in] X Row matrix of input data.
         * \param[in] Y Row matrix of target data.
         * \return Parameters of fitted line
         */
        Eigen::Vector2f simpleRegression(const Eigen::VectorXf &X, const Eigen::VectorXf &Y);
    };
}

#endif

